import { Box, Button, Typography } from '@mui/material'
import React, { memo } from 'react'

const Action = ({ text, onClick, ...props }) => {
  return (
    <Box>
      <Button onClick={() => {
        onClick();

      }}
      {...props}
      >
        <Typography variant='button'>
          {text}
        </Typography>
      </Button>
    </Box>
  )
}

export default memo(Action);
