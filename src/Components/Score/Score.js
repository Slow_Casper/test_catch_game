import { Typography } from '@mui/material';
import React, { memo } from 'react';

const Score = ({ score }) => {
  
  return (
    <Typography variant='subtitle1' >
      SCORE: {score}
    </Typography>
  )
}

export default memo(Score);
