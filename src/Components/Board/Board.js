import React, { memo } from 'react'
import { Box } from '@mui/material';
import Action from '../Action/Action';
import { boardButton, boardContainer, buttonsContainer, gameOver } from './styles';
import { useDispatch, useSelector } from 'react-redux';
import { GAME_STATUSES } from '../../helper';
import { setBoardArray, setScoreCounter } from '../../redux/slices/intervalSlice';
import { addScore } from '../../redux/slices/scoreSlice';

const Board = ({ setButton }) => {
  const dispatch = useDispatch();
  const gameStatus = useSelector(state => state.score.gameStatus);
  const board = useSelector(state => state.interval.board);
      
  const nextBtn = () => {
    dispatch(setBoardArray());
    dispatch(addScore());
    dispatch(setScoreCounter());
    setButton();
  };

  return (
    <Box sx={boardContainer}>
      {board.map((row, rowIndex) => (
        <Box key={rowIndex} sx={buttonsContainer}>
          {row.map(({ isActive }, colIndex) => (
            <Action
              key={colIndex}
              disabled={!isActive}
              variant="outlined"
              onClick={nextBtn}
              sx={gameStatus === GAME_STATUSES.ended ? gameOver : boardButton}
            />
          ))}
        </Box>
      ))}
    </Box>
  );
};

export default memo(Board)
