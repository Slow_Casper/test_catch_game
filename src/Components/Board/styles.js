export const boardContainer = {
    display: 'flex',
    flexDirection: 'column',
    gap: {
        mobile: '1px',
        lgTablet: '5px',
    },
};

export const buttonsContainer = {
    display: 'flex',
    gap: {
        mobile: '1px',
        lgTablet: '5px',
    },
};

export const boardButton = {
    width: {
        mobile: '20px',
        lgTablet: '30px',
    },
    height: {
        mobile: '20px',
        lgTablet: '30px',
    },
    borderRadius: '50%'
};

export const gameOver = {
    ...boardButton,
    backgroundColor: '#d21453',
}
