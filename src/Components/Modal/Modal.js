import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Slide, Typography } from '@mui/material'
import React, { forwardRef } from 'react'
import Action from '../Action/Action';
import { useSelector } from 'react-redux';

const Transition = forwardRef((props, ref) => {
    return <Slide direction="down" ref={ref} {...props} />;
  });

const Modal = ({ close, data }) => {
    const { title, content } = data;

    const gameStatus = useSelector(state => state.score.gameStatus)

    return (
        <Dialog
            open={gameStatus === 'ended'}
            TransitionComponent={Transition}
            keepMounted
            onClose={close}
            aria-describedby='dialog-text'
            sx={{ opacity: '0.8', backdropFilter: 'blur(10px)' }}
        >
            <DialogTitle >
                <Typography variant='h1'>
                    {title}
                </Typography>
            </DialogTitle>
            <DialogContent>
                <DialogContentText id='dialog-text' >
                    {content}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Action text='restart' onClick={close} sx={{ opacity: 1, backgroundColor: 'red' }} />
            </DialogActions>
        </Dialog>
    )
}

export default Modal
