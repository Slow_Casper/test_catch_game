  import { Box, Container } from "@mui/material";
  import Board from "./Components/Board/Board";
  import Score from "./Components/Score/Score";
  import Action from "./Components/Action/Action";
  import Modal from './Components/Modal/Modal'
  import { useDispatch, useSelector } from "react-redux";
  import { resetScore, setGameStatus } from "./redux/slices/scoreSlice";
  import { memo, useCallback, useEffect, useState } from "react";
  import { resetBoardArray, resetInterval, setBoardArray, setRemainingTime, setStartTime } from "./redux/slices/intervalSlice";
  import { action, actionScore, actionsContainer, app, container } from "./style";
  import { GAME_STATUSES, data } from './helper'

  function App() {
    const dispatch = useDispatch();
    const { score, gameStatus } = useSelector(state => state.score);
    const { intervalTime, remainingTime } = useSelector(state => state.interval);

    const [ timeoutId, setTimeoutId ] = useState(null);

    const chooseBtn = useCallback(() => {
      if(GAME_STATUSES.paused) {
        dispatch(setGameStatus(GAME_STATUSES.started))
      }
      dispatch(setStartTime())
      dispatch(setBoardArray());

      clearTimeout(timeoutId);

      const newTimeoutId = setTimeout(() => {
        dispatch(setGameStatus(GAME_STATUSES.ended));
      }, intervalTime);

      setTimeoutId(newTimeoutId);

    }, [dispatch, timeoutId, intervalTime])
    
    const start = () => {
      dispatch(setGameStatus(GAME_STATUSES.started))
      chooseBtn();
    };

    const pause = useCallback(() => {
      dispatch(setGameStatus(GAME_STATUSES.paused));
      dispatch(setRemainingTime());
      clearTimeout(timeoutId);
    }, [dispatch, timeoutId]);
    
    const resume = () => {
      dispatch(setGameStatus(GAME_STATUSES.started));
      const newTimeoutId = setTimeout(() => {
        dispatch(setGameStatus(GAME_STATUSES.ended));
      }, remainingTime);
      setTimeoutId(newTimeoutId);
    }
    
    const reset = () => {
      clearTimeout(timeoutId);
      dispatch(resetScore());
      dispatch(resetInterval())
      dispatch(resetBoardArray());
    };
    
    const closeModal = () => {
      dispatch(setGameStatus(GAME_STATUSES.initial));
      reset();
    };

    useEffect(() => {
      if(score > 0){
        window.addEventListener('beforeunload', pause)
      }
      
      return () => window.addEventListener('beforeunload', pause)
    }, [score, pause]);

    return (
      <Container
        sx={container}
      >
        <Box className="App"
          sx={app}
        >
          <Board setButton={() => chooseBtn()} />
          <Box
            sx={actionScore}
          >
            <Score score={score} />
            <Box sx={actionsContainer}>
              {gameStatus === GAME_STATUSES.initial && <Action text='Start' onClick={start} sx={action} />}
              {gameStatus === GAME_STATUSES.started && <Action text='Pause' onClick={pause} sx={action} />}
              {gameStatus === GAME_STATUSES.paused && <Action text='Resume' onClick={resume} sx={action} />}
              <Action text='Reset' onClick={reset} sx={action} />
            </Box>
          </Box>
        </Box>
        {gameStatus === GAME_STATUSES.ended && <Modal data={data.modalData} close={closeModal} />}
      </Container>
    );
  }

  export default memo(App);
