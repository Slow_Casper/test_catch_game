export const container = {
    display: 'flex',
    justifyContent: 'center',
};

export const app = {
    mt: '100px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: 'max-content'
};

export const actionScore = {
    width: '100%',
    padding: '10px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: '16px',
    backgroundColor: 'rgba(255, 246, 144, 0.6)',
    mt: '50px',
};

export const actionsContainer = {
    display: 'flex',
    gap: '20px'
};

export const action = {
    backgroundColor: 'button.secondary',
    width: '100px',
    height: '40px',
};
