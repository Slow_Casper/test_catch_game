export const GAME_STATUSES = {
    initial: 'initial',
    started: 'started',
    paused: 'paused',
    ended: 'ended',
};

export const data = {
    modalData: {
      title: 'GAME OVER',
      content: 'Sorry, but your game is over. Try again!'
    },
};