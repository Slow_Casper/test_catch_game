import { createTheme } from '@mui/material/styles';

const globalTheme = createTheme({
  breakpoints: {
    values: {
      mobile: 0,
      tablet: 481,
      lgTablet: 690,
      desktop: 993,
    },
  },
  palette: {
    button: {
      main: '#775c6e',
      secondary: '#EAEAEA',
      // hover: '#664FFF',
      disabled: '#fff590',
    },
  },
  typography: {
    fontPoppins: 'Poppins, sans-serif',
    fontWeightBold: 700,
  },
  components: {
    MuiButton: {
      defaultProps: {
        disableRipple: true,
      },
      styleOverrides: {
        root: {
          border: '1px dashed #0000',
          color: '#0000',
          minWidth: 'auto',
          backgroundColor: '#775c6e',
          transition: 'background-color 0.3s ease, box-shadow 0.3s ease',
          '&.Mui-disabled': {
            border: '1px dashed #0000',
            backgroundColor: '#fff590',
            opacity: '.6',
            pointerEvents: 'none',
          },
          '&:hover': {
            boxShadow: '3px 3px 10px rgba(255, 255, 255, .4)',
            backgroundColor: 'rgba(0, 0, 255, .4)',
          },
          '&:active': {
            boxShadow: '5px 5px 10px rgba(255, 255, 255, .8)',
            transform: 'translateY(1px)',
          },
        },
      },
    },
  },
});

globalTheme.typography.h1 = {
  lineHeight: '2em',
  color: '#000',
};

globalTheme.typography.subtitle1 = {
  lineHeight: '1.5em',
  fontSize: '18px',
  [globalTheme.breakpoints.up('tablet')]: {
    fontSize: '24px',
  },
};

globalTheme.typography.button = {
  fontWeight: 500,
  lineHeight: '1.5em',
  fontSize: '16px',
  color: '#000',
  [globalTheme.breakpoints.up('tablet')]: {
    fontSize: '20px',
  },
  '&: hover': {
    textShadow: '2px 2px 2px #fff590'
  }
};

export default globalTheme;