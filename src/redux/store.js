import { configureStore, combineReducers } from '@reduxjs/toolkit';
import storage from 'redux-persist/lib/storage'
import scoreSlice from './slices/scoreSlice';
import { persistReducer } from 'redux-persist';
import intervalSlice from './slices/intervalSlice';

const scorePersistConfig = {
    key: 'score',
    version: 1,
    storage,
};

const intervalPersistConfig = {
    key: 'interval',
    version: 1,
    storage,
};

const reducer = combineReducers({
    score: persistReducer(scorePersistConfig, scoreSlice),
    interval: persistReducer(intervalPersistConfig, intervalSlice),
});

const store = configureStore({
    reducer,
    middleware: (getDefaultMiddlefare) => (
        getDefaultMiddlefare({
            serializableCheck: false,
        })
    ),
});

export default store;
