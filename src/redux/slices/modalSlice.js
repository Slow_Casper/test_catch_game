import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isModalOpen: false,
};

const modalSlice = createSlice({
    name: 'modal',
    initialState,
    reducers: {
        setModalOpen(state, action) {
            state.isModalOpen = action.payload;
        },
    },
});

export const { setModalOpen } = modalSlice.actions;

export default modalSlice.reducer;
