import { createSlice } from "@reduxjs/toolkit";
import { GAME_STATUSES } from "../../helper";

const initialState = {
    score: 0,
    gameStatus: GAME_STATUSES.initial,
};

const scoreSlice = createSlice({
    name: 'score',
    initialState,
    reducers: {
        setGameStatus(state, action) {
            state.gameStatus = action.payload;
        },
        addScore(state) {
            state.score++;
        },
        resetScore(state) {
            Object.assign(state, {
                score: 0,
                gameStatus: GAME_STATUSES.initial,
            });
        },
    },
});

export const { setGameStatus, addScore, resetScore } = scoreSlice.actions;

export default scoreSlice.reducer;
