import { createSlice } from "@reduxjs/toolkit";


const initialState = {
    intervalTime: 3000,
    board: Array(10).fill(null).map(() => Array(10).fill(null).map(() => ({ isActive: false }))),
    startTime: 0,
    remainingTime: 0,
    scoreCounter: 0,
};

const intervalSlice = createSlice({
    name: 'interval',
    initialState,
    reducers: {
        setScoreCounter(state) {
            if(state.scoreCounter === 3) {
                state.scoreCounter = 0;
                state.intervalTime = Math.max(state.intervalTime - 300, 100);
            } else {
                state.scoreCounter++;
            };
        },
        resetInterval(state) {
            Object.assign(state, {
                intervalTime: 3000,
                scoreCounter: 0,
            });
        },
        setStartTime(state) {
            state.startTime = Date.now();
        },
        setRemainingTime(state) {
            const time = Date.now() - state.startTime;
            state.remainingTime = state.intervalTime - time;
        },
        setBoardArray(state) {
            state.board = Array(10).fill(null).map(() => Array(10).fill(null).map(() => ({ isActive: false })));

            const randomRow = Math.floor(Math.random() * 10);
            const randomCol = Math.floor(Math.random() * 10);

            state.board[randomRow][randomCol].isActive = true;
        },
        resetBoardArray(state) {
            state.board = Array(10).fill(null).map(() => Array(10).fill(null).map(() => ({ isActive: false })));
        },
    },
});

export const { setScoreCounter, resetInterval, setStartTime, setRemainingTime, setBoardArray, resetBoardArray } = intervalSlice.actions;

export default intervalSlice.reducer;
